import pandas as pd
import numpy as np
import torch
from city import City
from bot import Bot


class Net(torch.nn.Module):
    def __init__(self, input_size: int, hidden_size: int, output_size: int):
        super(Net, self).__init__()
        self.net = torch.nn.Sequential(
            torch.nn.Linear(input_size, hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(hidden_size, output_size),
        )

    def forward(self, x):
        return self.net(x)


class BotCrossEntropy(Bot):
    def __init__(self, env: City, nb_episodes: int = 100, percentile: int = 70):
        super().__init__(env)
        self.index: int = 0
        self.nb_episodes: int = nb_episodes
        self.percentile: int = percentile
        self.episodes: pd.DataFrame = pd.DataFrame(columns=['id', 'reward', 'observation', 'action'])
        self.obs_size: int = len(self.env.observations)
        assert self.obs_size == 15
        self.action_size: int = len(self.env.actions)
        assert self.action_size == 3
        self.net: Net = Net(input_size=self.obs_size, hidden_size=30, output_size=self.action_size)
        self.optimizer: torch.optim.Adam = torch.optim.Adam(self.net.parameters(), lr=0.01)
        self.objective = torch.nn.CrossEntropyLoss()

    def play_episodes(self, verbose: bool = False):
        sm = torch.nn.Softmax(dim=1)
        for i in range(self.nb_episodes):
            observations: list = []
            actions: list = []
            obs = self.env.reset()
            is_done = False
            while not is_done:
                pred_actions_tmp = sm(self.net(torch.FloatTensor([obs])))
                pred_actions = pred_actions_tmp.data.numpy()[0]
                action = np.random.choice(self.action_size, p=pred_actions)
                real_action = self.env.actions[action]
                new_obs, _, is_done = self.env.step(real_action)
                observations.append(obs)
                actions.append(action)
                if verbose:
                    print(self.env)
                    print("Score: ", self.env.score)  # , " Reward: ", total_reward)
                    import time
                    import os
                    time.sleep(0.5)
                    os.system('cls' if os.name == 'nt' else 'clear')

                if is_done:
                    list_id = [self.index for _ in range(len(actions))]
                    list_reward = [self.env.score for _ in range(len(actions))]
                    tmp = pd.DataFrame(list(zip(list_id, list_reward, observations, actions)),
                                       columns=['id', 'reward', 'observation', 'action'])
                    self.episodes = pd.concat([self.episodes, tmp])
                    self.index += 1
                else:
                    obs = new_obs

    def compute_bound_and_mean(self):
        uniq_id = self.episodes.groupby(['id']).max()['reward']
        return uniq_id.quantile(self.percentile / 100.0), uniq_id.mean()

    def filter_episodes(self):
        reward_bound, reward_mean = self.compute_bound_and_mean()
        self.episodes = self.episodes[self.episodes['reward'] >= reward_bound]
        train = self.episodes[self.episodes['reward'] >= reward_bound]
        x_train = train['observation']
        y_train = torch.LongTensor(train['action'].values.astype(int))
        x_train = pd.DataFrame(item for item in x_train)
        t_x_train = torch.FloatTensor(x_train.values.astype(float))
        return t_x_train, y_train, reward_bound, reward_mean

    def train(self, x_train, y_train, reward_bound, reward_mean, verbose: bool = False):
        self.optimizer.zero_grad()
        actions_scores_v = self.net(x_train)
        loss_v = self.objective(actions_scores_v, y_train)
        loss_v.backward()
        self.optimizer.step()
        if verbose:
            print("Ite=%d : loss=%.3f, reward_mean=%.1f, reward_bound=%.1f" % (
                self.index // self.nb_episodes, loss_v.item(), reward_mean, reward_bound))

    def learn(self, verbose: bool = False, nb_epochs: int = 1000):
        for i in range(nb_epochs):
            self.play_episodes(False)
            x_train, y_train, r_b, r_m = self.filter_episodes()
            self.train(x_train, y_train, r_b, r_m, verbose)

    def gen_move(self):
        x = torch.FloatTensor([self.env.generate_observations()])
        move = self.net(x)
        return self.env.actions[torch.argmax(move).item()]