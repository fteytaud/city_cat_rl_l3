from bot import Bot
from city import City
import copy
import numpy as np


class BotMonteCarlo(Bot):
    def __init__(self, env: City, nb_sims:int = 2000):
        super(BotMonteCarlo, self).__init__(env)
        self.nb_simulations: int = nb_sims

    def learn(self, verbose: bool = False):
        pass

    def gen_move(self):
        rewards: np.list = []
        for action in self.env.actions:
            reward: int = 0
            for _ in range(self.nb_simulations):
                copy_env = copy.deepcopy(self.env)
                _, _, is_done = copy_env.step(action)
                while not is_done:
                    moves = copy_env.actions
                    m = np.random.choice(moves)
                    _, _, is_done = copy_env.step(m)
                reward += copy_env.score
            rewards.append(reward / self.nb_simulations)
        return self.env.actions[np.argmax(rewards)]
