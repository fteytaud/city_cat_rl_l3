from city import City


class Bot:
    def __init__(self, env: City):
        self.env = env

    def learn(self, verbose: bool = False):
        raise NotImplementedError("Subclass must implement abstract method")

    def gen_move(self):
        raise NotImplementedError("Subclass must implement abstract method")
