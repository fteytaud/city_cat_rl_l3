# Projet pour le module d'intelligence artificielle pour les étudiants de L3

Le but de ces TPs vont être de faire une petite initiation à l'apprentissage par renforcement.

## Rappel

Pour rappel voici un lien vers les commandes git utiles pour ce module :
https://juliendehos.gitlab.io/posts/env/post20-git.html#forker-un-d%C3%A9p%C3%B4t-existant

## Etape 1 : Installation

- Forker ce dépôt.

Les paquets nécessaires sont `numpy`, `pandas` et `torch`.

- Sous linux, vous pouvez utiliser conda par exemple :

Rendez-vous ici https://docs.conda.io/en/latest/miniconda.html#linux-installers
pour installer miniconda, ensuite :

```bash
	conda create --name "ia_l3" python=3.8 pip
	conda activate ia_l3
	cd city_cat_rl_l3
	pip install .
```

- Sous linux, avec un ide comme pycharm par exemple :

```bash
	cd city_cat_rl_l3
	pycharm .&
```

Dès l'ouverture, pycharm va automatiquement détecter le setup et vous proposer de créer un environnement virtuel

- Sous windows:

Le plus simple est d'utiliser anaconda et de créer l'environnement à la main. Utiliser ensuite un des ide proposés et se
laisser guider par la détection automatique du setup.

## Etape 2 : Compréhension de l'environnement

Prendre en main le code dans le fichier src/City.py

## Etape 3 : Création d'un bot aléatoire

Comprendre le fonctionnement de la classe abstraite src/Bot.py

Compléter le bot random. Il se trouve dans le fichier src/BotRandom.py

## Etape 4 : Création d'un bot expert

A l'aide de quelques heuristiques, créer un bot expert. Je vous laisse libre dans le choix de votre stratégie, mais
inutile de chercher quelque chose de très pointu ici.

## Etape 5 : Création d'un bot Monte-Carlo

Créer un bot "BotMonteCarlo.py". Le bot est très simple :

- créer une liste vide qui servira à contenir les scores moyens de chaque action
- pour chaque action *act* possible :
    - créer une variable temporaire de score
    - pour chaque simulation :
        - créer une copie de l'environnement (deepcopy)
        - jouer *act* et tant que la simulation n'est pas terminée :
            - choisir une action aléatoirement
            - jouer cette action
        - récupérer le score obtenu, et l'ajouter à la variable temporaire
    - ajouter le score temporaire à la liste des scores
- renvoyer l'action qui a obtenu le meilleur score moyen

## Etape 6 : Apprendre un réseau de neurones avec du Monte-Carlo

Comme vous avez pu le constater, sur ce petit problème, la méthode de Monte-Carlo donne de bons résultats. Le problème
est que nous devons refaire des simulations pour chaque décision.

Il serait intéressant d'apprendre à partir de notre Monte-Carlo afin d'avoir un bot qui, pour chaque décision soit
capable
de choisir une action sans refaire de simulations. C'est ce que nous allons faire ici.

Tout d'abord nous allons devoir créer un réseau de neurones. Pour l'instant nous ne nous intéresserons pas en détail à
leur fonctionnement ; nous allons les voir comme une fonction ''boite noire'' qui va faire un mapping entre les actions
et des observations : ``proba_actions = reseau_neurones(observations)``.

Nous allons créer le réseau avec pytorch. Voici sa définition :

```python
class Net(torch.nn.Module):
    def __init__(self, input_size: int, hidden_size: int, output_size: int):
        super(Net, self).__init__()
        self.net = torch.nn.Sequential(
            torch.nn.Linear(input_size, hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(hidden_size, output_size),
        )

    def forward(self, x):
        return self.net(x)
```

Ce qu'on peut noter :

- nous avons autant de neurones d'entrées que d'observations (```input_size```).
- nous avons autant de sorties que d'actions possibles (```output_size```).

Vous verrez le fonctionnement en détail des réseaux en master dans le module d'apprentissage automatique (pour les
curieux : https://www-lisic.univ-littoral.fr/~teytaud/apprentissage.html).

Voici l'idée algorithmique de notre bot pour cette étape :

- Comme pour le Monte-Carlo, nous allons effectuer un certain nombre de simulations, sauf que, au lieu de prendre des
  décisions purement aléatoires, nous allons être guidé par le réseau de neurones.
- Une fois qu'un certain nombre de simulations seront faites, parmi l'ensemble des simulations nous allons garder
  uniquement les k-meilleures.
- Nous allons *entrainer* notre réseau de neurones avec cet échantillon des meilleures simulations.
- On réitère ces étapes tant qu'on a du temps.

# Examen

- Les questions sont indépendantes, si vous êtes bloqués sur une question, ne perdez pas de temps et passez à la
  suivante.
- Vous devez répondre dans votre fichier README de votre fork en indiquant à chaque fois le numéro de la question.

## Question Cours/Compréhension des TPs

Expliquez le principe du botMonteCarlo. Quels sont ses avantages et ses inconvénients.

## Question 1

Créer un Bot `BotEpsilonGreedy` ayant deux attributs `nb_simulations` et `epsilon`.
Ce bot aura un comportement similaire au botMonteCarlo, à la différence qu'au lieu de toujours faire une action
aléatoire, il va :

- avec une probabilité `epsilon`, avoir un comportement expert (vous pouvez vous inspirer de votre bot expert).
- avec une probabilité `1 - epsilon`, faire une action aléatoire.

Pour avoir un nombre aléatoire réel entre 0 et 1, vous pouvez utiliser `np.random.random()`.

## Question 2

Modifiez la fonction `run_all_algos` pour y ajouter ce bot et enlever le bot `BotCrossEntropy`.
Testez ces algos en faisant des moyennes sur 30 parties minimum.

## Question 3

Dans la fonction `check_collisions`, enlevez ou commentez le bonus de score `+10` lorsque le chat mange de la nourriture.
Retestez vos algos. Essayez d'expliquer pourquoi cela semble plus compliqué.

## Question 4 (bonus - difficile)

Rajouter une fonction dans la classe `city` qui permet de faire une copy de classe uniquement à partir des
observations. Il faut régénérer une map aléatoirement (comme `reset`) en ne gardant que les cases observées de la classe
d'origine.
Les `score`, `direction`, `done` peuvent aussi être récupérés. La position du chat est au centre de la ville, et les
ajouts des batiments, des chiens et de la nourriture sont aussi similaires à la fonction `reset`.
